import connect
import cmd
import sys
import json
from pprint import pprint
import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning

props = dict(line.strip().split('=') for line in open('env.properties'))

## uDeploy Connection Params
url=props['udeploy.url']
user=props['udeploy.username']
token=props['udeploy.token']

# Disable insecure SSL Warnings
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

def create_ms_component(appName):
    # Create a new component based on above params
    new_component_template= {
                        "name": appName,
                        "description": appName,
                        "templateId": "dd316a06-6f94-4ab9-b343-512bf42f04da",
                        "templateVersion": "",
                        "componentType": "STANDARD",
                        "sourceConfigPlugin": "",
                        "importAutomatically": "false",
                        "useVfs": "true",
                        "defaultVersionType": "FULL",
                        "importAgentType": "inherit",
                        "inheritSystemCleanup": "true",
                        "runVersionCreationProcess": "false",
                        "properties": {},
                        "importProperties": {
                        "properties": {}
                            },
                        #"teamMappings": [{
                        #"teamId": "aeb05534-eddc-405d-beb8-bcfb1544c3ce"
                            }]
                        }

    head = {'Content-type':'application/json',
            'Accept':'application/json'
           }

    ud=connect.udeploy(url,user,token)
    res=ud.uput('/cli/component/create',new_component_template,head)
    if res.status_code != 200:
        print ("[ERROR] Some error occured while creating the new Component {0}".format(appName))
        print (res.text)
        return False
    else:
        print ("[INFO] New Component {0} Created Successfully".format(appName))
        create_component_json_response=json.loads(res.text)
        component_id=create_component_json_response['id']
        return component_id

if __name__ == "__main__":

    print ("******************uDeploy Onboarding - Applications******************")
    appName=input("Enter the name of the application : ")
    print ("[INFO] Kicking off onboarding")
    component_id=create_ms_component(appName)
    if component_id:
        print ("[Info] Component Created successfully")
    else:
        print ("[ERROR] Some error occured while creating components.")
    print ("***********************************************************************")
